package com.kazakimaru.ch04t5

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.kazakimaru.ch04t5.adapter.StudentActionListener
import com.kazakimaru.ch04t5.adapter.StudentAdapter
import com.kazakimaru.ch04t5.database.Student
import com.kazakimaru.ch04t5.database.StudentDatabase
import com.kazakimaru.ch04t5.databinding.FragmentCrudBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class CrudFragment : Fragment() {

    private var _binding: FragmentCrudBinding? = null
    private val binding get() = _binding!!

    private lateinit var studentAdapter: StudentAdapter

    private var mDB: StudentDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCrudBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDB = StudentDatabase.getInstance(requireContext())
        initRecyclerView()
        getDataFromDb()
        addStudent()
    }

    private fun initRecyclerView() {
        binding.apply {
            studentAdapter = StudentAdapter(
//                {},
//                {},
                action
            )
            rvData.adapter = studentAdapter
            rvData.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun addStudent() {
        binding.fabAdd.setOnClickListener {
            showCustomDialog(null)
        }
    }

    private fun showCustomDialog(student: Student?) {
        // Menghubungkan layout
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_dialog_add, null, false)

        // Mmebuat builder AlertDialog
        val dialogBuilder = AlertDialog.Builder(requireContext())

        // Memanggil textview dan button dari custom layout
        val etNama = customLayout.findViewById<EditText>(R.id.et_name)
        val etEmail = customLayout.findViewById<EditText>(R.id.et_email)
        val btnSave = customLayout.findViewById<Button>(R.id.btn_save)

        // Tampilkan value dari database pada EditText di UpdateDialog
        if (student != null) {
            etNama.setText(student.namaaa)
            etEmail.setText(student.emailll)
        }

        // Mengubah layout AlertDialog menggunakan custom layout
        dialogBuilder.setView(customLayout)

        // Membuat AlertDialog baru dari builder yang sudah di custom layout
        val dialog = dialogBuilder.create()

        // Action pada Button Save
        btnSave.setOnClickListener {
            val nama = etNama.text.toString()
            val email = etEmail.text.toString()

            if (student != null) {
                val newStudent = Student(student.id, nama, email)
                updateToDb(newStudent)
            } else {
                saveToDb(nama, email)
            }


            dialog.dismiss()
        }

        dialog.show()
    }

    private fun saveToDb(name: String, email: String) {
        val student = Student(null, name, email)
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDB?.studentDao()?.insertStudent(student)
            if (result != 0L) {
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Berhasil ditambahkan", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Gagal ditambahkan", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun updateToDb(student: Student) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDB?.studentDao()?.updateStudent(student)
            if (result != 0) {
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Berhasil diperbarui", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Gagal diperbarui", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun getDataFromDb() {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDB?.studentDao()?.getAllStudent()
            if (result != null) {
                CoroutineScope(Dispatchers.Main).launch {
                    studentAdapter.updateData(result)
                }
            }
        }
    }

    private fun deleteItemDb(student: Student) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDB?.studentDao()?.deleteStudent(student)
            if (result != 0) {
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Berhasil dihapus", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Gagal dihapus", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private val action = object : StudentActionListener {
        override fun onDelete(student: Student) {
            deleteItemDb(student)
        }

        override fun onEdit(student: Student) {
            showCustomDialog(student)
        }

    }

}