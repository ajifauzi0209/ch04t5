package com.kazakimaru.ch04t5.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kazakimaru.ch04t5.R
import com.kazakimaru.ch04t5.database.Student

class StudentAdapter(
//        private val onDelete : (Student) -> Unit,
//        private val onEdit : (Student) -> Unit,
        private val listener : StudentActionListener
    ) : RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {

    val diffCallback = object : DiffUtil.ItemCallback<Student>() {
        override fun areItemsTheSame(oldItem: Student, newItem: Student): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Student, newItem: Student): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun updateData(students: List<Student>) = differ.submitList(students)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_data, parent, false)
        return StudentViewHolder(view)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class StudentViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val tvId = view.findViewById<TextView>(R.id.tv_id_value)
        val tvName = view.findViewById<TextView>(R.id.tv_name_value)
        val tvEmail = view.findViewById<TextView>(R.id.tv_email_value)
        val btnDelete = view.findViewById<ImageView>(R.id.btn_delete)
        val btnEdit = view.findViewById<ImageView>(R.id.btn_edit)

        fun bind(student: Student) {
            tvId.text = student.id.toString()
            tvName.text = student.namaaa
            tvEmail.text = student.emailll

            btnDelete.setOnClickListener {
//                onDelete.invoke(student)
                listener.onDelete(student)
            }

            btnEdit.setOnClickListener {
//                onEdit.invoke(student)
                listener.onEdit(student)
            }
        }
    }

}

interface StudentActionListener {
    fun onDelete(student: Student)
    fun onEdit(student: Student)
}
