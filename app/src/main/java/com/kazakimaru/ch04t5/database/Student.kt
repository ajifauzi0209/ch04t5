package com.kazakimaru.ch04t5.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Student(
    @PrimaryKey(autoGenerate = true) var id : Int?,
    @ColumnInfo(name = "nama") val namaaa: String,
    @ColumnInfo(name = "email") val emailll: String
)




